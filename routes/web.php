<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Factories\AppleEventsFactory;
use App\Factories\PurchaseEventsFactoryInterface;
use App\Http\Controllers\NotificationController;
use App\Processors\AppleEventProcessor;
use App\Processors\PurchaseEventProcessorInterface;
use App\Validators\AppleValidator;
use App\Validators\PurchaseValidatorInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/notify/{provider}', function (string $provider, Request $request) use ($router) {
    switch ($provider) {
        case 'apple':
            $router->app->bind(PurchaseValidatorInterface::class, AppleValidator::class);
            $router->app->bind(PurchaseEventsFactoryInterface::class, AppleEventsFactory::class);
            $router->app->bind(PurchaseEventProcessorInterface::class, AppleEventProcessor::class);
            break;
        default:
            return new JsonResponse(['message' => "provider $provider is not supported"], 400);
    }

    /** @var NotificationController $controller */
    $controller = $router->app->make(NotificationController::class);

    return $controller->notify($request);
});

<?php


namespace App\Factories;


use App\PurchaseEvents\PurchaseEvent;

interface PurchaseEventsFactoryInterface
{
    public function createEventFromPayload(array $event): PurchaseEvent;
}

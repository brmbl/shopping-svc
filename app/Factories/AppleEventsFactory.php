<?php


namespace App\Factories;


use App\PurchaseEvents\PurchaseEvent;

class AppleEventsFactory implements PurchaseEventsFactoryInterface
{

    public function createEventFromPayload(array $event): PurchaseEvent
    {
        $eventType = strtolower($event['notification_type']);
        $eventTypeParts = explode('_', $eventType);
        $eventTypeParts = array_map(static function ($item) {
            return ucfirst($item);
        }, $eventTypeParts);
        $className = '\App\PurchaseEvents\Apple\\'.implode('', $eventTypeParts);
        $eventObject = new $className($event);
        if (!class_implements($eventObject, PurchaseEvent::class)) {
            throw new \InvalidArgumentException("event {$event['notification_type']} is not supported");
        }

        return $eventObject;
    }
}

<?php


namespace App\PurchaseEvents\Apple;


class InitialBuy extends AppleEvent
{
    public const EVENT_NAME = 'INITIAL_BUY';
}

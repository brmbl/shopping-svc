<?php


namespace App\PurchaseEvents\Apple;


class DidRenew extends AppleEvent
{
    public const EVENT_NAME = 'DID_RENEW';
}

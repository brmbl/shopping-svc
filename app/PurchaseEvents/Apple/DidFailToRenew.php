<?php


namespace App\PurchaseEvents\Apple;


class DidFailToRenew extends AppleEvent
{
    public const EVENT_NAME = 'DID_FAIL_TO_RENEW';
}

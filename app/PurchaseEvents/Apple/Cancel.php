<?php


namespace App\PurchaseEvents\Apple;


class Cancel extends AppleEvent
{
    public const EVENT_NAME = 'CANCEL';
}

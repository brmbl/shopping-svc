<?php


namespace App\PurchaseEvents\Apple;


use App\PurchaseEvents\PurchaseEvent;

class AppleEvent implements PurchaseEvent
{
    private array $payload;

    public function __construct(array $payload)
    {
        $this->payload = $payload;
    }

    public function getEventName(): string
    {
        return static::EVENT_NAME;
    }

    public function getEventData(): array
    {
        return $this->payload;
    }

    /**
     * @return string
     * @throws \JsonException
     */
    public function __toString(): string
    {
        return json_encode($this->payload, JSON_THROW_ON_ERROR);
    }
}

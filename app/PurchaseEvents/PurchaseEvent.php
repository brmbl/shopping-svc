<?php

namespace App\PurchaseEvents;

interface PurchaseEvent
{
    public function getEventName(): string;

    public function getEventData(): array;
}

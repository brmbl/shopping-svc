<?php

namespace App\Validators;

interface PurchaseValidatorInterface
{
    public function validatePayload(array $payload): array;
}

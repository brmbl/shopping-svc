<?php

namespace App\Validators;

use Illuminate\Support\Facades\Validator;

class AppleValidator implements PurchaseValidatorInterface
{
    //TODO:: MOVE TO APPLE EVENT OBJECT
    private const ALLOWED_STATUSES = ['INITIAL_BUY', 'DID_RENEW', 'DID_FAIL_TO_RENEW', 'CANCEL'];

    public function validatePayload(array $payload): array
    {
        $validator = Validator::make($payload, [
            'latest_receipt' => 'required|uuid',
            'notification_type' => 'required|in:'.implode(',', self::ALLOWED_STATUSES),
        ]);
        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }

        return [];
    }
}

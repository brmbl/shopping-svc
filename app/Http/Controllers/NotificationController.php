<?php

namespace App\Http\Controllers;

use App\Factories\PurchaseEventsFactoryInterface;
use App\Processors\PurchaseEventProcessorInterface;
use App\Validators\PurchaseValidatorInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    private PurchaseValidatorInterface $validator;

    private PurchaseEventsFactoryInterface $eventsFactory;

    private PurchaseEventProcessorInterface $eventProcessor;

    public function __construct(
        PurchaseValidatorInterface $validator,
        PurchaseEventsFactoryInterface $eventsFactory,
        PurchaseEventProcessorInterface $eventProcessor
    ) {
        $this->validator = $validator;
        $this->eventsFactory = $eventsFactory;
        $this->eventProcessor = $eventProcessor;
    }

    public function notify(Request $request): JsonResponse
    {
        $payload = $request->input();
        $validationResult = $this->validator->validatePayload($payload);
        if (!empty($validationResult)) {
            return new JsonResponse($validationResult, 400);
        }

        $eventObject = $this->eventsFactory->createEventFromPayload($payload);
        $this->eventProcessor->process($eventObject);

        return new JsonResponse(['success' => true], 200);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Transaction extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected array $fillable = [
        'created_at',
        'payment_provider',
        'payment_data',
    ];

    public $timestamps = false;

    public function subscription(): HasOne
    {
        return $this->hasOne(Subscription::class);
    }


}

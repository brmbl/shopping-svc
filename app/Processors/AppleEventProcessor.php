<?php


namespace App\Processors;


use App\Models\Subscription;
use App\Models\Transaction;
use App\PurchaseEvents\Apple\Cancel;
use App\PurchaseEvents\Apple\DidFailToRenew;
use App\PurchaseEvents\Apple\DidRenew;
use App\PurchaseEvents\Apple\InitialBuy;
use App\PurchaseEvents\PurchaseEvent;
use Illuminate\Support\Facades\DB;

class AppleEventProcessor implements PurchaseEventProcessorInterface
{

    public function process(PurchaseEvent $event): void
    {
        switch ($event->getEventName()) {
            case InitialBuy::EVENT_NAME:
                $this->processInitialBuy($event);
                break;
            case DidRenew::EVENT_NAME:
                //todo: process
                break;
            case DidFailToRenew::EVENT_NAME:
                //todo: process
                break;
            case Cancel::EVENT_NAME:
                //todo: process
                break;
            default:
                throw new \InvalidArgumentException("Cannot process {$event->getEventName()} event");
        }
    }

    private function processInitialBuy(PurchaseEvent $event): void
    {
        $transaction = new Transaction();
        $transaction->created_at = new \DateTime();
        $transaction->payment_provider = 'apple';
        $transaction->payment_data = (string)$event;

        $subscription = new Subscription();
        $subscription->user_id = $event->getEventData()['latest_receipt'];
        $subscription->expire_at = new \DateTime($event->getEventData()['latest_receipt_info']['expires_date_formatted']);

        DB::transaction(function () use ($transaction, $subscription) {
            $transaction = $transaction->save();

            //TODO: use not sqlite DB then $transaction->id will be accesible instead of this workarround
            $lastInsertObj = DB::select('SELECT last_insert_rowid()');
            $lastInsert = (array)$lastInsertObj[0];
            $id = (int)$lastInsert['last_insert_rowid()'];
            Transaction::find($id)->subscription()->save($subscription);
        });
    }
}

<?php

namespace App\Processors;

use App\PurchaseEvents\PurchaseEvent;

interface PurchaseEventProcessorInterface
{
    public function process(PurchaseEvent $event): void;
}
